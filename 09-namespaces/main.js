/*
╔╗╔┌─┐┌┬┐┌─┐┌─┐┌─┐┌─┐┌─┐┌─┐┌─┐  ┬ ┬  ┌┬┐┌─┐┌┬┐┬ ┬┬  ┌─┐┌─┐
║║║├─┤│││├┤ └─┐├─┘├─┤│  ├┤ └─┐  └┬┘  ││││ │ │││ ││  │ │└─┐
╝╚╝┴ ┴┴ ┴└─┘└─┘┴  ┴ ┴└─┘└─┘└─┘   ┴   ┴ ┴└─┘─┴┘└─┘┴─┘└─┘└─┘
*/
console.log('********** Namespaces **********');
var texto = "Barry Allen";
console.log("texto", texto);
if (Validaciones.validarTexto(texto)) {
    console.log("El texto es válido");
}
else {
    console.log("El texto no es válido");
}
var fecha = new Date();
console.log("fecha", fecha);
if (Validaciones.validarFecha(fecha)) {
    console.log("El fecha es válida");
}
else {
    console.log("El fecha no es válida");
}
