"use strict";
var Validaciones;
(function (Validaciones) {
    function validarTexto(texto) {
        if (texto.length > 3) {
            return true;
        }
        return false;
    }
    Validaciones.validarTexto = validarTexto;
})(Validaciones || (Validaciones = {}));
var Validaciones;
(function (Validaciones) {
    function validarFecha(fecha) {
        if (isNaN(fecha.valueOf())) {
            return false;
        }
        return true;
    }
    Validaciones.validarFecha = validarFecha;
})(Validaciones || (Validaciones = {}));
/* Importando namespaces */
/// <reference path="validaciones/textos.ts" />
/// <reference path="validaciones/fechas.ts" />
/*
╔╗╔┌─┐┌┬┐┌─┐┌─┐┌─┐┌─┐┌─┐┌─┐┌─┐  ┬ ┬  ┌┬┐┌─┐┌┬┐┬ ┬┬  ┌─┐┌─┐
║║║├─┤│││├┤ └─┐├─┘├─┤│  ├┤ └─┐  └┬┘  ││││ │ │││ ││  │ │└─┐
╝╚╝┴ ┴┴ ┴└─┘└─┘┴  ┴ ┴└─┘└─┘└─┘   ┴   ┴ ┴└─┘─┴┘└─┘┴─┘└─┘└─┘
*/
console.log('********** Namespaces **********');
var texto = "Barry Allen";
console.log("texto", texto);
if (Validaciones.validarTexto(texto)) {
    console.log("El texto es válido");
}
else {
    console.log("El texto no es válido");
}
var fecha = new Date();
console.log("fecha", fecha);
if (Validaciones.validarFecha(fecha)) {
    console.log("El fecha es válida");
}
else {
    console.log("El fecha no es válida");
}
