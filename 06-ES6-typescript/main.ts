/*
╔╦╗┌─┐┌─┐┬  ┌─┐┬─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┬  ┬┌─┐┬─┐┬┌─┐┌┐ ┬  ┌─┐┌─┐  ┌─┐┌─┐┌┐┌  ┬  ┌─┐┌┬┐
 ║║├┤ │  │  ├─┤├┬┘├─┤│  ││ ││││   ││├┤   └┐┌┘├─┤├┬┘│├─┤├┴┐│  ├┤ └─┐  │  │ ││││  │  ├┤  │
═╩╝└─┘└─┘┴─┘┴ ┴┴└─┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘   └┘ ┴ ┴┴└─┴┴ ┴└─┘┴─┘└─┘└─┘  └─┘└─┘┘└┘  ┴─┘└─┘ ┴
*/
console.log('********** Declaración de variables con let **********');

let nombre = "Tony";

if (true) {
    let nombre = "Bruce";

    if (true) {
        let nombre = "Ricardo";
    }
}

console.log(nombre);

/*
┌┬┐┌─┐┌─┐┬  ┌─┐┬─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┬  ┬┌─┐┬─┐┬┌─┐┌┐ ┬  ┌─┐┌─┐  ┌─┐┌─┐┌┐┌  ┌─┐┌─┐┌┐┌┌─┐┌┬┐
 ││├┤ │  │  ├─┤├┬┘├─┤│  ││ ││││   ││├┤   └┐┌┘├─┤├┬┘│├─┤├┴┐│  ├┤ └─┐  │  │ ││││  │  │ ││││└─┐ │
─┴┘└─┘└─┘┴─┘┴ ┴┴└─┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘   └┘ ┴ ┴┴└─┴┴ ┴└─┘┴─┘└─┘└─┘  └─┘└─┘┘└┘  └─┘└─┘┘└┘└─┘ ┴
*/
console.log('********** Declaración de variables con const **********');

const OPCIONES: string = "Activo";

if (true) {
    const OPCIONES: string = "Desactivado";
}

for (const i of [1, 2, 3, 4, 5]) {
    console.log("i = " + i);
}

const OBJETO = {
    nombre: 'Tony',
    edad: 45,
    poder: "armadura"
}

console.log('OBJETO: ', OBJETO);

OBJETO.nombre = "Logan";
OBJETO.edad = 0;
OBJETO.poder = "Regeneración inmediata";

console.log('OBJETO: ', OBJETO);

/*
╔╦╗┌─┐┌┬┐┌─┐┬  ┌─┐┌┬┐┌─┐┌─┐  ┬  ┬┌┬┐┌─┐┬─┐┌─┐┬  ┌─┐┌─┐
 ║ ├┤ │││├─┘│  ├─┤ │ ├┤ └─┐  │  │ │ ├┤ ├┬┘├─┤│  ├┤ └─┐
 ╩ └─┘┴ ┴┴  ┴─┘┴ ┴ ┴ └─┘└─┘  ┴─┘┴ ┴ └─┘┴└─┴ ┴┴─┘└─┘└─┘
*/
console.log('********** Templates literales **********');

let nombre1: string = "Bruce";
let nombre2: string = "Dick Grayson";

function getNombres(): string {
    return `${nombre1} y ${nombre2}`

}

let mensaje: string = `1. esta es una línea normal
2. Hola ${nombre1}
3. Robin es ${nombre2}
4. Los nombres son ${getNombres()}
5. 5 + 7 = ${5 + 7}
`;

console.log(mensaje);

/*
╔═╗┬ ┬┌┐┌┌─┐┬┌─┐┌┐┌┌─┐┌─┐  ┌┬┐┌─┐  ┌─┐┬  ┌─┐┌─┐┬ ┬┌─┐  ┬  ┌─┐┌┬┐┌┐ ┌┬┐┌─┐
╠╣ │ │││││  ││ ││││├┤ └─┐   ││├┤   ├┤ │  ├┤ │  ├─┤├─┤  │  ├─┤│││├┴┐ ││├─┤
╚  └─┘┘└┘└─┘┴└─┘┘└┘└─┘└─┘  ─┴┘└─┘  └  ┴─┘└─┘└─┘┴ ┴┴ ┴  ┴─┘┴ ┴┴ ┴└─┘─┴┘┴ ┴
*/
console.log('********** Funciones de flecha o lambda **********');

// function sumar(a: number, b: number): number {
//     return a + b;
// }

let sumar = (a: number, b: number) => a + b;

console.log('sumar:', sumar(5, 7));

// function darOrdenHulk(orden:string):string{
//     return `Hulk ${orden}`;
// }

let darOrdenHulk = (orden: string) => `Hulk ${orden}`;

console.log(darOrdenHulk('smash!!!'));

let capitanAmerica = {
    nombre: "Hulk",
    darOrden: function () {
        setTimeout(() => console.log(this.nombre + " smash!!!"), 1000);
    }

}
capitanAmerica.darOrden();

/*
╔╦╗┌─┐┌─┐┌┬┐┬─┐┬ ┬┌─┐┌┬┐┬ ┬┬─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┌─┐┌┐  ┬┌─┐┌┬┐┌─┐┌─┐
 ║║├┤ └─┐ │ ├┬┘│ ││   │ │ │├┬┘├─┤│  ││ ││││   ││├┤   │ │├┴┐ │├┤  │ │ │└─┐
═╩╝└─┘└─┘ ┴ ┴└─└─┘└─┘ ┴ └─┘┴└─┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘  └─┘└─┘└┘└─┘ ┴ └─┘└─┘
*/
console.log('********** Destructuración de objetos **********');

let avengers = {
    nick: "Samuel Jackson",
    ironman: "Robert Downey Jr.",
    vision: "Paul Bettany"
}

// let ironman =  avengers.ironman;
// let nick =  avengers.nick;
// let vision =  avengers.vision;

let { nick, ironman: sherlock, vision } = avengers;

console.log("sherlock", sherlock)

/*
┌┬┐┌─┐┌─┐┌┬┐┬─┐┬ ┬┌─┐┌┬┐┬ ┬┬─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┌─┐┬─┐┬─┐┌─┐┌─┐┬  ┌─┐┌─┐
 ││├┤ └─┐ │ ├┬┘│ ││   │ │ │├┬┘├─┤│  ││ ││││   ││├┤   ├─┤├┬┘├┬┘├┤ │ ┬│  │ │└─┐
─┴┘└─┘└─┘ ┴ ┴└─└─┘└─┘ ┴ └─┘┴└─┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘  ┴ ┴┴└─┴└─└─┘└─┘┴─┘└─┘└─┘
*/
console.log('********** Destructuración de arreglos **********');
let arrAvengers = ["Samuel L. Jackson", "Robert Downey Jr.", "Paul Bettany"];

// let [avenger1, avenger2, avenger3] = arrAvengers;
// let [, avenger2] = arrAvengers;
let [, , avenger3] = arrAvengers;
// console.log("avenger1", avenger1)
// console.log("avenger2", avenger2)
console.log("avenger3", avenger3);

/*
┬┌┬┐┌─┐┬─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┌─┐┌┐  ┬┌─┐┌┬┐┌─┐┌─┐
│ │ ├┤ ├┬┘├─┤│  ││ ││││   ││├┤   │ │├┴┐ │├┤  │ │ │└─┐
┴ ┴ └─┘┴└─┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘  └─┘└─┘└┘└─┘ ┴ └─┘└─┘
*/
console.log('********** Iteración de objetos **********');

let thor = {
    nombre: "Thor",
    arma: "Mjölnir"
}

let ironman = {
    nombre: "Iron Man",
    arma: "Armour suit"
}

let capitan = {
    nombre: "Capitán america",
    arma: "Escudo"
}

let avengers1 = [thor, ironman, capitan];

console.log('forin->');
for (const i in avengers1) {
    let avenger = avengers1[i];
    console.log(avenger.nombre, avenger.arma);
}

console.log('for->');
for (let i = 0; i < avengers1.length; i++) {
    const element = avengers1[i];
    console.log(element.nombre, element.arma);    
}

console.log('forof->');
for (const avenger of avengers1) {
    console.log(avenger.nombre, avenger.arma);
}