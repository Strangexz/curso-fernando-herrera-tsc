"use strict";
/*
╔╦╗┌─┐┌─┐┬  ┌─┐┬─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┬  ┬┌─┐┬─┐┬┌─┐┌┐ ┬  ┌─┐┌─┐  ┌─┐┌─┐┌┐┌  ┬  ┌─┐┌┬┐
 ║║├┤ │  │  ├─┤├┬┘├─┤│  ││ ││││   ││├┤   └┐┌┘├─┤├┬┘│├─┤├┴┐│  ├┤ └─┐  │  │ ││││  │  ├┤  │
═╩╝└─┘└─┘┴─┘┴ ┴┴└─┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘   └┘ ┴ ┴┴└─┴┴ ┴└─┘┴─┘└─┘└─┘  └─┘└─┘┘└┘  ┴─┘└─┘ ┴
*/
console.log('********** Declaración de variables con let **********');
var nombre = "Tony";
if (true) {
    var nombre_1 = "Bruce";
    if (true) {
        var nombre_2 = "Ricardo";
    }
}
console.log(nombre);
/*
┌┬┐┌─┐┌─┐┬  ┌─┐┬─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┬  ┬┌─┐┬─┐┬┌─┐┌┐ ┬  ┌─┐┌─┐  ┌─┐┌─┐┌┐┌  ┌─┐┌─┐┌┐┌┌─┐┌┬┐
 ││├┤ │  │  ├─┤├┬┘├─┤│  ││ ││││   ││├┤   └┐┌┘├─┤├┬┘│├─┤├┴┐│  ├┤ └─┐  │  │ ││││  │  │ ││││└─┐ │
─┴┘└─┘└─┘┴─┘┴ ┴┴└─┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘   └┘ ┴ ┴┴└─┴┴ ┴└─┘┴─┘└─┘└─┘  └─┘└─┘┘└┘  └─┘└─┘┘└┘└─┘ ┴
*/
console.log('********** Declaración de variables con const **********');
var OPCIONES = "Activo";
if (true) {
    var OPCIONES_1 = "Desactivado";
}
for (var _i = 0, _a = [1, 2, 3, 4, 5]; _i < _a.length; _i++) {
    var i = _a[_i];
    console.log("i = " + i);
}
var OBJETO = {
    nombre: 'Tony',
    edad: 45,
    poder: "armadura"
};
console.log('OBJETO: ', OBJETO);
OBJETO.nombre = "Logan";
OBJETO.edad = 0;
OBJETO.poder = "Regeneración inmediata";
console.log('OBJETO: ', OBJETO);
/*
╔╦╗┌─┐┌┬┐┌─┐┬  ┌─┐┌┬┐┌─┐┌─┐  ┬  ┬┌┬┐┌─┐┬─┐┌─┐┬  ┌─┐┌─┐
 ║ ├┤ │││├─┘│  ├─┤ │ ├┤ └─┐  │  │ │ ├┤ ├┬┘├─┤│  ├┤ └─┐
 ╩ └─┘┴ ┴┴  ┴─┘┴ ┴ ┴ └─┘└─┘  ┴─┘┴ ┴ └─┘┴└─┴ ┴┴─┘└─┘└─┘
*/
console.log('********** Templates literales **********');
var nombre1 = "Bruce";
var nombre2 = "Dick Grayson";
function getNombres() {
    return nombre1 + " y " + nombre2;
}
var mensaje = "1. esta es una l\u00EDnea normal\n2. Hola " + nombre1 + "\n3. Robin es " + nombre2 + "\n4. Los nombres son " + getNombres() + "\n5. 5 + 7 = " + (5 + 7) + "\n";
console.log(mensaje);
/*
╔═╗┬ ┬┌┐┌┌─┐┬┌─┐┌┐┌┌─┐┌─┐  ┌┬┐┌─┐  ┌─┐┬  ┌─┐┌─┐┬ ┬┌─┐  ┬  ┌─┐┌┬┐┌┐ ┌┬┐┌─┐
╠╣ │ │││││  ││ ││││├┤ └─┐   ││├┤   ├┤ │  ├┤ │  ├─┤├─┤  │  ├─┤│││├┴┐ ││├─┤
╚  └─┘┘└┘└─┘┴└─┘┘└┘└─┘└─┘  ─┴┘└─┘  └  ┴─┘└─┘└─┘┴ ┴┴ ┴  ┴─┘┴ ┴┴ ┴└─┘─┴┘┴ ┴
*/
console.log('********** Funciones de flecha o lambda **********');
// function sumar(a: number, b: number): number {
//     return a + b;
// }
var sumar = function (a, b) { return a + b; };
console.log('sumar:', sumar(5, 7));
// function darOrdenHulk(orden:string):string{
//     return `Hulk ${orden}`;
// }
var darOrdenHulk = function (orden) { return "Hulk " + orden; };
console.log(darOrdenHulk('smash!!!'));
var capitanAmerica = {
    nombre: "Hulk",
    darOrden: function () {
        var _this = this;
        setTimeout(function () { return console.log(_this.nombre + " smash!!!"); }, 1000);
    }
};
capitanAmerica.darOrden();
/*
╔╦╗┌─┐┌─┐┌┬┐┬─┐┬ ┬┌─┐┌┬┐┬ ┬┬─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┌─┐┌┐  ┬┌─┐┌┬┐┌─┐┌─┐
 ║║├┤ └─┐ │ ├┬┘│ ││   │ │ │├┬┘├─┤│  ││ ││││   ││├┤   │ │├┴┐ │├┤  │ │ │└─┐
═╩╝└─┘└─┘ ┴ ┴└─└─┘└─┘ ┴ └─┘┴└─┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘  └─┘└─┘└┘└─┘ ┴ └─┘└─┘
*/
console.log('********** Destructuración de objetos **********');
var avengers = {
    nick: "Samuel Jackson",
    ironman: "Robert Downey Jr.",
    vision: "Paul Bettany"
};
// let ironman =  avengers.ironman;
// let nick =  avengers.nick;
// let vision =  avengers.vision;
var nick = avengers.nick, sherlock = avengers.ironman, vision = avengers.vision;
console.log("sherlock", sherlock);
/*
┌┬┐┌─┐┌─┐┌┬┐┬─┐┬ ┬┌─┐┌┬┐┬ ┬┬─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┌─┐┬─┐┬─┐┌─┐┌─┐┬  ┌─┐┌─┐
 ││├┤ └─┐ │ ├┬┘│ ││   │ │ │├┬┘├─┤│  ││ ││││   ││├┤   ├─┤├┬┘├┬┘├┤ │ ┬│  │ │└─┐
─┴┘└─┘└─┘ ┴ ┴└─└─┘└─┘ ┴ └─┘┴└─┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘  ┴ ┴┴└─┴└─└─┘└─┘┴─┘└─┘└─┘
*/
console.log('********** Destructuración de arreglos **********');
var arrAvengers = ["Samuel L. Jackson", "Robert Downey Jr.", "Paul Bettany"];
// let [avenger1, avenger2, avenger3] = arrAvengers;
// let [, avenger2] = arrAvengers;
var avenger3 = arrAvengers[2];
// console.log("avenger1", avenger1)
// console.log("avenger2", avenger2)
console.log("avenger3", avenger3);
/*
┬┌┬┐┌─┐┬─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┌─┐┌┐  ┬┌─┐┌┬┐┌─┐┌─┐
│ │ ├┤ ├┬┘├─┤│  ││ ││││   ││├┤   │ │├┴┐ │├┤  │ │ │└─┐
┴ ┴ └─┘┴└─┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘  └─┘└─┘└┘└─┘ ┴ └─┘└─┘
*/
console.log('********** Iteración de objetos **********');
var thor = {
    nombre: "Thor",
    arma: "Mjölnir"
};
var ironman = {
    nombre: "Iron Man",
    arma: "Armour suit"
};
var capitan = {
    nombre: "Capitán america",
    arma: "Escudo"
};
var avengers1 = [thor, ironman, capitan];
console.log('forin->');
for (var i in avengers1) {
    var avenger = avengers1[i];
    console.log(avenger.nombre, avenger.arma);
}
console.log('for->');
for (var i = 0; i < avengers1.length; i++) {
    var element = avengers1[i];
    console.log(element.nombre, element.arma);
}
console.log('forof->');
for (var _b = 0, avengers1_1 = avengers1; _b < avengers1_1.length; _b++) {
    var avenger = avengers1_1[_b];
    console.log(avenger.nombre, avenger.arma);
}
