"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
function consola(constructor) {
    console.log('********** Decorador de clase **********');
    console.log(constructor);
}
function imprimirConsola(imprimir) {
    console.log('********** Decorador de fabrica **********');
    if (imprimir) {
        return consola;
    }
    else {
        return function () { };
    }
}
function planDelVillano(constructor) {
    console.log('********** Otro decorador **********');
    constructor.prototype.imprimirPlan = function () {
    };
}
function imprimible(constructor) {
    console.log('********** Decorador anidado **********');
    constructor.prototype.imprimir = function () {
        console.log(this);
    };
}
function editable(esEditable) {
    console.log('********** Decorador de método **********');
    return function (target, propertieName, descriptor) {
        descriptor.writable = esEditable;
    };
}
function editableProp(esEditable) {
    console.log('********** Decorador de propiedad **********');
    return function (target, propertieName) {
        var descriptor = {
            writable: esEditable
        };
        return descriptor;
    };
}
function parametros(target, methodName, index) {
    console.log('********** Decorador de parámetro **********');
    console.log(target, methodName, index);
}
var Villano = (function () {
    function Villano(nombre, poder) {
        this.nombre = nombre;
    }
    Villano.prototype.plan = function () { };
    ;
    Villano.prototype.imprimir = function (plan, mensaje) {
        if (plan) {
            console.log("el plan es " + mensaje);
        }
        else {
            console.log(mensaje);
        }
    };
    ;
    __decorate([
        editableProp(true)
    ], Villano.prototype, "nombre", void 0);
    __decorate([
        editable(true)
    ], Villano.prototype, "plan", null);
    __decorate([
        __param(1, parametros)
    ], Villano.prototype, "imprimir", null);
    return Villano;
}());
var lex = new Villano("Lex Luthor", "Inteligencia");
lex.plan = function () {
};
lex.plan();
console.log(lex);
