/*
┌┬┐┌─┐┌─┐┌─┐┬─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐  ┌┬┐┌─┐  ┌─┐┬  ┌─┐┌─┐┌─┐
 ││├┤ │  │ │├┬┘├─┤ │││ │├┬┘├┤ └─┐   ││├┤   │  │  ├─┤└─┐├┤
─┴┘└─┘└─┘└─┘┴└─┴ ┴─┴┘└─┘┴└─└─┘└─┘  ─┴┘└─┘  └─┘┴─┘┴ ┴└─┘└─┘
*/

function consola(constructor: Function): void {
    console.log('********** Decorador de clase **********');

    console.log(constructor);
}

/*
┌┬┐┌─┐┌─┐┌─┐┬─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐  ┌┬┐┌─┐  ┌─┐┌─┐┌┐ ┬─┐┬┌─┐┌─┐
 ││├┤ │  │ │├┬┘├─┤ │││ │├┬┘├┤ └─┐   ││├┤   ├┤ ├─┤├┴┐├┬┘││  ├─┤
─┴┘└─┘└─┘└─┘┴└─┴ ┴─┴┘└─┘┴└─└─┘└─┘  ─┴┘└─┘  └  ┴ ┴└─┘┴└─┴└─┘┴ ┴
*/
function imprimirConsola(imprimir: boolean): Function {
    console.log('********** Decorador de fabrica **********');

    if (imprimir) {
        return consola;
    } else {
        return () => { };
    }
}

/*
┌─┐ ┬┌─┐┌┬┐┌─┐┬  ┌─┐  ┌┬┐┌─┐  ┌┬┐┌─┐┌─┐┌─┐┬─┐┌─┐┌┬┐┌─┐┬─┐
├┤  │├┤ │││├─┘│  │ │   ││├┤    ││├┤ │  │ │├┬┘├─┤ │││ │├┬┘
└─┘└┘└─┘┴ ┴┴  ┴─┘└─┘  ─┴┘└─┘  ─┴┘└─┘└─┘└─┘┴└─┴ ┴─┴┘└─┘┴└─
*/
function planDelVillano(constructor: Function) {
    console.log('********** Otro decorador **********');

    constructor.prototype.imprimirPlan = function () {
        // console.log(this);
    }
}

/*
┌┬┐┌─┐┌─┐┌─┐┬─┐┌─┐┌┬┐┌─┐┬─┐  ┌─┐┌┐┌┬┌┬┐┌─┐┌┬┐┌─┐
 ││├┤ │  │ │├┬┘├─┤ │││ │├┬┘  ├─┤││││ ││├─┤ │││ │
─┴┘└─┘└─┘└─┘┴└─┴ ┴─┴┘└─┘┴└─  ┴ ┴┘└┘┴─┴┘┴ ┴─┴┘└─┘
*/
function imprimible(constructor: Function) {
    console.log('********** Decorador anidado **********');

    constructor.prototype.imprimir = function () {
        console.log(this);
    }
}

/*
┌┬┐┌─┐┌─┐┌─┐┬─┐┌─┐┌┬┐┌─┐┬─┐  ┌┬┐┌─┐  ┌┬┐┌─┐┌┬┐┌─┐┌┬┐┌─┐
 ││├┤ │  │ │├┬┘├─┤ │││ │├┬┘   ││├┤   │││├┤  │ │ │ │││ │
─┴┘└─┘└─┘└─┘┴└─┴ ┴─┴┘└─┘┴└─  ─┴┘└─┘  ┴ ┴└─┘ ┴ └─┘─┴┘└─┘
*/
function editable(esEditable: boolean) {
    console.log('********** Decorador de método **********');

    return function (target: any, propertieName: string, descriptor: PropertyDescriptor) {
        // if (!esEditable) {
        //     console.warn("No voy a cambiar de opinión");
        // } else {
        //     descriptor.writable = esEditable;
        // }
        descriptor.writable = esEditable;
    }
}

/*
┌┬┐┌─┐┌─┐┌─┐┬─┐┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐  ┌┬┐┌─┐  ┌─┐┬─┐┌─┐┌─┐┬┌─┐┌┬┐┌─┐┌┬┐┌─┐┌─┐
 ││├┤ │  │ │├┬┘├─┤ │││ │├┬┘├┤ └─┐   ││├┤   ├─┘├┬┘│ │├─┘│├┤  ││├─┤ ││├┤ └─┐
─┴┘└─┘└─┘└─┘┴└─┴ ┴─┴┘└─┘┴└─└─┘└─┘  ─┴┘└─┘  ┴  ┴└─└─┘┴  ┴└─┘─┴┘┴ ┴─┴┘└─┘└─┘
*/
function editableProp(esEditable: boolean) {
    console.log('********** Decorador de propiedad **********');

    return function (target: any, propertieName: string): any {
        let descriptor: PropertyDescriptor = {
            writable: esEditable
        }

        return descriptor;
    }
}

/*
┌┬┐┌─┐┌─┐┌─┐┬─┐┌─┐┌┬┐┌─┐┬─┐  ┌┬┐┌─┐  ┌─┐┌─┐┬─┐┌─┐┌┬┐┌─┐┌┬┐┬─┐┌─┐┌─┐
 ││├┤ │  │ │├┬┘├─┤ │││ │├┬┘   ││├┤   ├─┘├─┤├┬┘├─┤│││├┤  │ ├┬┘│ │└─┐
─┴┘└─┘└─┘└─┘┴└─┴ ┴─┴┘└─┘┴└─  ─┴┘└─┘  ┴  ┴ ┴┴└─┴ ┴┴ ┴└─┘ ┴ ┴└─└─┘└─┘
*/
function parametros(target: any, methodName: string, index: number): void {
    console.log('********** Decorador de parámetro **********');
    
    console.log(target, methodName, index);
}
class Villano {
    @editableProp(true)
    nombre: string;

    constructor(nombre: string, poder?: string) {
        this.nombre = nombre;
    }

    @editable(true)
    plan() { };

    imprimir(plan: boolean, @parametros mensaje: string): void {
        if (plan) {
            console.log("el plan es " + mensaje);
        } else {
            console.log(mensaje);
        }
    };
}

let lex = new Villano("Lex Luthor", "Inteligencia");
// (<any>lex).imprimirPlan();
// (<any>lex).imprimir();
lex.plan = function () {
};

lex.plan();

console.log(lex);
