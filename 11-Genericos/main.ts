/*
╔═╗┌─┐┌┐┌┌─┐┬─┐┬┌─┐┌─┐┌─┐
║ ╦├┤ │││├┤ ├┬┘││  │ │└─┐
╚═╝└─┘┘└┘└─┘┴└─┴└─┘└─┘└─┘
*/
console.log('********** Funciones Genéricas **********');

function regresar<T>(arg: T) {
    return arg;
}

console.log(regresar(15.564169849).toFixed(2));
console.log(regresar("Hola mundo!!!").charAt(0));
console.log(regresar(new Date()).getTime());

console.log('********** Otra función genérica **********');

function funcionGenerica<T>(arg: T) {
    return arg;
}

type Heroe = {
    nombre: string;
    nombreReal: string;
}

type Villano = {
    nombre: string;
    poder: string;
}

let deadpool = {
    nombre: "Deadpool",
    nombreReal: "Wade Wilson",
    poder: "Regeneración"
}

console.log(funcionGenerica<Villano>(deadpool).poder);

console.log('********** Arreglos Genéricos **********');
let heroes: Array<string> = ["Flash", "Batman", "Superman"];
console.log("heroes", heroes)

heroes.push("Aquaman");
console.log("heroes", heroes);

let villanos: string[] = ["Joker", "Doomsday", "Reverse Flash"];
console.log("villanos", villanos)

console.log('********** Clases Genéricas **********');
class Cuadrado<T extends number | string> {
    base: T;
    altura: T;
    area(): number {
        return +this.base * +this.altura;
    }

    constructor(base: T, altura: T) {
        this.base = base;
        this.altura = altura;
    }
}

let cuadrado = new Cuadrado<number|string>(10, "10");

// cuadrado.base = 10;
// cuadrado.altura = 10;
console.log(cuadrado.area());
