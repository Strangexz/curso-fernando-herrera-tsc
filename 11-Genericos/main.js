"use strict";
/*
╔═╗┌─┐┌┐┌┌─┐┬─┐┬┌─┐┌─┐┌─┐
║ ╦├┤ │││├┤ ├┬┘││  │ │└─┐
╚═╝└─┘┘└┘└─┘┴└─┴└─┘└─┘└─┘
*/
console.log('********** Funciones Genéricas **********');
function regresar(arg) {
    return arg;
}
console.log(regresar(15.564169849).toFixed(2));
console.log(regresar("Hola mundo!!!").charAt(0));
console.log(regresar(new Date()).getTime());
console.log('********** Otra función genérica **********');
function funcionGenerica(arg) {
    return arg;
}
var deadpool = {
    nombre: "Deadpool",
    nombreReal: "Wade Wilson",
    poder: "Regeneración"
};
console.log(funcionGenerica(deadpool).poder);
console.log('********** Arreglos Genéricos **********');
var heroes = ["Flash", "Batman", "Superman"];
console.log("heroes", heroes);
heroes.push("Aquaman");
console.log("heroes", heroes);
var villanos = ["Joker", "Doomsday", "Reverse Flash"];
console.log("villanos", villanos);
console.log('********** Clases Genéricas **********');
var Cuadrado = /** @class */ (function () {
    function Cuadrado(base, altura) {
        this.base = base;
        this.altura = altura;
    }
    Cuadrado.prototype.area = function () {
        return +this.base * +this.altura;
    };
    return Cuadrado;
}());
var cuadrado = new Cuadrado(10, "10");
// cuadrado.base = 10;
// cuadrado.altura = 10;
console.log(cuadrado.area());
