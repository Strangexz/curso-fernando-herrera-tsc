"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
╔═╗┬  ┌─┐┌─┐┌─┐  ┌┐ ┌─┐┌─┐┬┌─┐┌─┐  ┬ ┬  ┌─┐┌─┐┌─┐┌─┐┌─┐  ┌┬┐┌─┐  ┬  ┬┌─┐┬─┐┬┌─┐┌┐ ┬  ┌─┐┌─┐  ┬ ┬  ┌┬┐┌─┐┌┬┐┌─┐┌┬┐┌─┐┌─┐
║  │  ├─┤└─┐├┤   ├┴┐├─┤└─┐││  ├─┤  └┬┘  └─┐│  │ │├─┘├┤    ││├┤   └┐┌┘├─┤├┬┘│├─┤├┴┐│  ├┤ └─┐  └┬┘  │││├┤  │ │ │ │││ │└─┐
╚═╝┴─┘┴ ┴└─┘└─┘  └─┘┴ ┴└─┘┴└─┘┴ ┴   ┴   └─┘└─┘└─┘┴  └─┘  ─┴┘└─┘   └┘ ┴ ┴┴└─┴┴ ┴└─┘┴─┘└─┘└─┘   ┴   ┴ ┴└─┘ ┴ └─┘─┴┘└─┘└─┘
*/
console.log('********** Clase básica y scopes **********');
var Avenger = /** @class */ (function () {
    function Avenger(nombre, equipo, nombreReal) {
        this.puedePelear = false;
        this.peleasGanadas = 0;
        this.nombre = nombre;
        this.equipo = equipo;
        this.nombreReal = nombreReal;
    }
    Avenger.prototype.bio = function () {
        var mensaje = this.nombre + " " + this.nombreReal + " " + this.equipo;
        console.log(mensaje);
    };
    Avenger.prototype.cambiarEquipo = function (nuevoEquipo) {
        if (nuevoEquipo === this.equipo) {
            return false;
        }
        else {
            return true;
        }
    };
    Avenger.prototype.cambiarEquipoPublico = function (nuevoEquipo) {
        return this.cambiarEquipo(nuevoEquipo);
    };
    return Avenger;
}());
var antman = new Avenger("Antman", "#teamCap", "Scott Lang");
antman.bio();
console.log(antman.cambiarEquipoPublico('#teamIronman'));
/*
╦ ╦┌─┐┬─┐┌─┐┌┐┌┌─┐┬┌─┐
╠═╣├┤ ├┬┘├┤ ││││  │├─┤
╩ ╩└─┘┴└─└─┘┘└┘└─┘┴┴ ┴
*/
console.log('********** Herencia **********');
var Vengador = /** @class */ (function () {
    function Vengador(nombre, nombreReal) {
        this.nombre = nombre;
        this.nombreReal = nombreReal;
        console.log("Llamando al constructor Vengador");
    }
    Vengador.prototype.getNombre = function () {
        return this.nombre;
    };
    return Vengador;
}());
var Xmen = /** @class */ (function (_super) {
    __extends(Xmen, _super);
    function Xmen(a, b) {
        var _this = this;
        console.log('Llmando al constructor Xmen');
        _this = _super.call(this, a, b) || this;
        return _this;
    }
    Xmen.prototype.getName = function () {
        return _super.prototype.getNombre.call(this);
    };
    return Xmen;
}(Vengador));
var ciclope = new Xmen("Ciclope", "Scott Summers");
console.log(ciclope.getName());
/*
┌─┐┌─┐┌┬┐┌─┐  ┬ ┬  ┌─┐┌─┐┌┬┐┌─┐
│ ┬├┤  │ └─┐  └┬┘  └─┐├┤  │ └─┐
└─┘└─┘ ┴ └─┘   ┴   └─┘└─┘ ┴ └─┘
*/
console.log('********** Gets y Sets **********');
var Avenger1 = /** @class */ (function () {
    function Avenger1(nombre) {
        this._nombre = nombre || '';
    }
    Object.defineProperty(Avenger1.prototype, "getNombre", {
        get: function () {
            if (this._nombre) {
                return this._nombre;
            }
            else {
                return "No tiene un nombre";
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Avenger1.prototype, "setNombre", {
        set: function (nombre) {
            this._nombre = nombre;
        },
        enumerable: false,
        configurable: true
    });
    return Avenger1;
}());
var cyclops = new Avenger1();
console.log(cyclops.getNombre);
cyclops.setNombre = "Wolverine";
console.log(cyclops.getNombre);
/*
╔═╗┬─┐┌─┐┌─┐┬┌─┐┌┬┐┌─┐┌┬┐┌─┐┌─┐  ┬ ┬  ┌┬┐┌─┐┌┬┐┌─┐┌┬┐┌─┐┌─┐  ┌─┐┌─┐┌┬┐┌─┐┌┬┐┬┌─┐┌─┐┌─┐
╠═╝├┬┘│ │├─┘│├┤  ││├─┤ ││├┤ └─┐  └┬┘  │││├┤  │ │ │ │││ │└─┐  ├┤ └─┐ │ ├─┤ │ ││  │ │└─┐
╩  ┴└─└─┘┴  ┴└─┘─┴┘┴ ┴─┴┘└─┘└─┘   ┴   ┴ ┴└─┘ ┴ └─┘─┴┘└─┘└─┘  └─┘└─┘ ┴ ┴ ┴ ┴ ┴└─┘└─┘└─┘
*/
console.log('********** Propiedades y Métodos estáticos **********');
var Xmen1 = /** @class */ (function () {
    function Xmen1() {
    }
    Xmen1.crearXmen = function () {
        console.log("Se creó con un método estático");
        return new Xmen1();
    };
    Xmen1.nombre = "Wolverine";
    return Xmen1;
}());
console.log(Xmen1.nombre);
var wolverine2 = Xmen1.crearXmen();
console.log("wolverine2", wolverine2);
/*
┌─┐┬  ┌─┐┌─┐┌─┐┌─┐  ┌─┐┌┐ ┌─┐┌┬┐┬─┐┌─┐┌─┐┌┬┐┌─┐┌─┐
│  │  ├─┤└─┐├┤ └─┐  ├─┤├┴┐└─┐ │ ├┬┘├─┤│   │ ├─┤└─┐
└─┘┴─┘┴ ┴└─┘└─┘└─┘  ┴ ┴└─┘└─┘ ┴ ┴└─┴ ┴└─┘ ┴ ┴ ┴└─┘
*/
console.log('********** Clases Abstractas **********');
var Mutantes = /** @class */ (function () {
    function Mutantes(nombre, nombreReal) {
        this.nombre = nombre;
        this.nombreReal = nombreReal;
    }
    return Mutantes;
}());
var Xmen2 = /** @class */ (function (_super) {
    __extends(Xmen2, _super);
    function Xmen2() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Xmen2;
}(Mutantes));
var beast = new Xmen2("Beast", "Henry McCoy");
console.log(beast);
/*
╔═╗┌─┐┌┐┌┌─┐┌┬┐┬─┐┬ ┬┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐  ┌─┐┬─┐┬┬  ┬┌─┐┌┬┐┌─┐┌─┐
║  │ ││││└─┐ │ ├┬┘│ ││   │ │ │├┬┘├┤ └─┐  ├─┘├┬┘│└┐┌┘├─┤ │││ │└─┐
╚═╝└─┘┘└┘└─┘ ┴ ┴└─└─┘└─┘ ┴ └─┘┴└─└─┘└─┘  ┴  ┴└─┴ └┘ ┴ ┴─┴┘└─┘└─┘
*/
console.log('********** Constructores privados **********');
var Apocalipsis = /** @class */ (function () {
    function Apocalipsis(nombre) {
        this.nombre = nombre;
    }
    Apocalipsis.llamarApocalipsis = function () {
        if (!Apocalipsis.instancia) {
            Apocalipsis.instancia = new Apocalipsis("En Sabah Nur");
        }
        return Apocalipsis.instancia;
    };
    return Apocalipsis;
}());
var apocalipsis = Apocalipsis.llamarApocalipsis();
console.log("apocalipsis", apocalipsis);
