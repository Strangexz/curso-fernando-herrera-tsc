/*
╔═╗┬  ┌─┐┌─┐┌─┐  ┌┐ ┌─┐┌─┐┬┌─┐┌─┐  ┬ ┬  ┌─┐┌─┐┌─┐┌─┐┌─┐  ┌┬┐┌─┐  ┬  ┬┌─┐┬─┐┬┌─┐┌┐ ┬  ┌─┐┌─┐  ┬ ┬  ┌┬┐┌─┐┌┬┐┌─┐┌┬┐┌─┐┌─┐
║  │  ├─┤└─┐├┤   ├┴┐├─┤└─┐││  ├─┤  └┬┘  └─┐│  │ │├─┘├┤    ││├┤   └┐┌┘├─┤├┬┘│├─┤├┴┐│  ├┤ └─┐  └┬┘  │││├┤  │ │ │ │││ │└─┐
╚═╝┴─┘┴ ┴└─┘└─┘  └─┘┴ ┴└─┘┴└─┘┴ ┴   ┴   └─┘└─┘└─┘┴  └─┘  ─┴┘└─┘   └┘ ┴ ┴┴└─┴┴ ┴└─┘┴─┘└─┘└─┘   ┴   ┴ ┴└─┘ ┴ └─┘─┴┘└─┘└─┘
*/
console.log('********** Clase básica y scopes **********');

class Avenger {
    public nombre: string;
    protected equipo: string;
    private nombreReal: string;

    private puedePelear: boolean = false;
    private peleasGanadas: number = 0;

    constructor(nombre: string, equipo: string, nombreReal: string) {
        this.nombre = nombre;
        this.equipo = equipo;
        this.nombreReal = nombreReal;
    }

    public bio(): void {
        let mensaje: string = `${this.nombre} ${this.nombreReal} ${this.equipo}`;
        console.log(mensaje);
    }

    private cambiarEquipo(nuevoEquipo: string): boolean {
        if (nuevoEquipo === this.equipo) {
            return false;
        } else {
            return true;
        }
    }

    public cambiarEquipoPublico(nuevoEquipo: string): boolean {
        return this.cambiarEquipo(nuevoEquipo);
    }

}

let antman: Avenger = new Avenger("Antman", "#teamCap", "Scott Lang");

antman.bio();
console.log(antman.cambiarEquipoPublico('#teamIronman'));

/*
╦ ╦┌─┐┬─┐┌─┐┌┐┌┌─┐┬┌─┐
╠═╣├┤ ├┬┘├┤ ││││  │├─┤
╩ ╩└─┘┴└─└─┘┘└┘└─┘┴┴ ┴
*/
console.log('********** Herencia **********');

class Vengador {
    constructor(public nombre: string, private nombreReal: string) {
        console.log("Llamando al constructor Vengador");

    }

    protected getNombre(): string {
        return this.nombre;
    }
}

class Xmen extends Vengador {
    constructor(a: string, b: string) {
        console.log('Llmando al constructor Xmen');
        super(a, b);
    }

    public getName(): string {
        return super.getNombre();
    }
}

let ciclope: Xmen = new Xmen("Ciclope", "Scott Summers");

console.log(ciclope.getName());

/*
┌─┐┌─┐┌┬┐┌─┐  ┬ ┬  ┌─┐┌─┐┌┬┐┌─┐
│ ┬├┤  │ └─┐  └┬┘  └─┐├┤  │ └─┐
└─┘└─┘ ┴ └─┘   ┴   └─┘└─┘ ┴ └─┘
*/
console.log('********** Gets y Sets **********');
class Avenger1 {
    private _nombre: string;

    constructor(nombre?: string) {
        this._nombre = nombre || '';

    }

    get getNombre(): string {
        if (this._nombre) {
            return this._nombre;
        } else {
            return "No tiene un nombre";
        }
    }

    set setNombre(nombre: string) {
        this._nombre = nombre;
    }
}

let cyclops: Avenger1 = new Avenger1();

console.log(cyclops.getNombre);
cyclops.setNombre = "Wolverine";
console.log(cyclops.getNombre);

/*
╔═╗┬─┐┌─┐┌─┐┬┌─┐┌┬┐┌─┐┌┬┐┌─┐┌─┐  ┬ ┬  ┌┬┐┌─┐┌┬┐┌─┐┌┬┐┌─┐┌─┐  ┌─┐┌─┐┌┬┐┌─┐┌┬┐┬┌─┐┌─┐┌─┐
╠═╝├┬┘│ │├─┘│├┤  ││├─┤ ││├┤ └─┐  └┬┘  │││├┤  │ │ │ │││ │└─┐  ├┤ └─┐ │ ├─┤ │ ││  │ │└─┐
╩  ┴└─└─┘┴  ┴└─┘─┴┘┴ ┴─┴┘└─┘└─┘   ┴   ┴ ┴└─┘ ┴ └─┘─┴┘└─┘└─┘  └─┘└─┘ ┴ ┴ ┴ ┴ ┴└─┘└─┘└─┘
*/
console.log('********** Propiedades y Métodos estáticos **********');

class Xmen1 {
    static nombre: string = "Wolverine";

    constructor() {

    }

    static crearXmen() {
        console.log("Se creó con un método estático");
        return new Xmen1();
    }
}

console.log(Xmen1.nombre);
let wolverine2 = Xmen1.crearXmen();
console.log("wolverine2", wolverine2);

/*
┌─┐┬  ┌─┐┌─┐┌─┐┌─┐  ┌─┐┌┐ ┌─┐┌┬┐┬─┐┌─┐┌─┐┌┬┐┌─┐┌─┐
│  │  ├─┤└─┐├┤ └─┐  ├─┤├┴┐└─┐ │ ├┬┘├─┤│   │ ├─┤└─┐
└─┘┴─┘┴ ┴└─┘└─┘└─┘  ┴ ┴└─┘└─┘ ┴ ┴└─┴ ┴└─┘ ┴ ┴ ┴└─┘
*/
console.log('********** Clases Abstractas **********');

abstract class Mutantes {
    constructor(public nombre: string, public nombreReal: string) {

    }
}

class Xmen2 extends Mutantes {

}

let beast = new Xmen2("Beast", "Henry McCoy");

console.log(beast);

/*
╔═╗┌─┐┌┐┌┌─┐┌┬┐┬─┐┬ ┬┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐  ┌─┐┬─┐┬┬  ┬┌─┐┌┬┐┌─┐┌─┐
║  │ ││││└─┐ │ ├┬┘│ ││   │ │ │├┬┘├┤ └─┐  ├─┘├┬┘│└┐┌┘├─┤ │││ │└─┐
╚═╝└─┘┘└┘└─┘ ┴ ┴└─└─┘└─┘ ┴ └─┘┴└─└─┘└─┘  ┴  ┴└─┴ └┘ ┴ ┴─┴┘└─┘└─┘
*/
console.log('********** Constructores privados **********');

class Apocalipsis {
    static instancia: Apocalipsis;

    private constructor(public nombre: string) { }

    static llamarApocalipsis() {
        if (!Apocalipsis.instancia) {
            Apocalipsis.instancia = new Apocalipsis("En Sabah Nur");
        }

        return Apocalipsis.instancia;
    }
}

let apocalipsis = Apocalipsis.llamarApocalipsis();
console.log("apocalipsis", apocalipsis)

