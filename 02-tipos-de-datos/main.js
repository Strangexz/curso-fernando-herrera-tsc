"use strict";
/*
 
   ____              _
  | __ )  ___   ___ | | ___  __ _ _ __   ___  ___
  |  _ \ / _ \ / _ \| |/ _ \/ _` | '_ \ / _ \/ __|
  | |_) | (_) | (_) | |  __/ (_| | | | | (_) \__ \
  |____/ \___/ \___/|_|\___|\__,_|_| |_|\___/|___/
                                                  
 
*/
console.log('***** Booleanos *****');
var esSuperman = true;
var esBAtman;
var esAcuaman = true;
if (esSuperman) {
    console.log("estamos salvados");
}
else {
    console.log(" estamos pisados");
}
esSuperman = convertitClark();
function convertitClark() {
    return false;
}
/*
 
   _   _   __
  | \ | |_/_/_ _ __ ___   ___ _ __ ___  ___
  |  \| | | | | '_ ` _ \ / _ \ '__/ _ \/ __|
  | |\  | |_| | | | | | |  __/ | | (_) \__ \
  |_| \_|\__,_|_| |_| |_|\___|_|  \___/|___/
                                            
 
*/
console.log('***** Números *****');
var avengers = 5;
var villanos = 0;
var otros = 2;
if (avengers > villanos) {
    console.log("estamos salvados");
}
else {
    console.log(" estamos pisados");
}
/*
 
    ____          _                            _                                 _
   / ___|__ _  __| | ___ _ __   __ _ ___    __| | ___    ___ __ _ _ __ __ _  ___| |_ ___ _ __ ___  ___
  | |   / _` |/ _` |/ _ \ '_ \ / _` / __|  / _` |/ _ \  / __/ _` | '__/ _` |/ __| __/ _ \ '__/ _ \/ __|
  | |__| (_| | (_| |  __/ | | | (_| \__ \ | (_| |  __/ | (_| (_| | | | (_| | (__| ||  __/ | |  __/\__ \
   \____\__,_|\__,_|\___|_| |_|\__,_|___/  \__,_|\___|  \___\__,_|_|  \__,_|\___|\__\___|_|  \___||___/
                                                                                                       
 
*/
console.log('***** Cadenas de caracteres *****');
var batman = "Batman";
var linternaVerde = 'Linterna Verde';
var volcanNegro = "Volc\u00E1n Negro";
console.log(batman);
console.log(linternaVerde);
console.log(volcanNegro);
var concatenar = "Los héroes son: " + batman + ", " + linternaVerde + volcanNegro;
console.log(concatenar);
var concat = "Los h\u00E9roes son " + batman + ", " + linternaVerde + ", " + volcanNegro;
console.log(concat);
/*
 
   _____ _                   _            _       _
  |_   _(_)_ __   ___     __| | ___    __| | __ _| |_ ___     __ _ _ __  _   _
    | | | | '_ \ / _ \   / _` |/ _ \  / _` |/ _` | __/ _ \   / _` | '_ \| | | |
    | | | | |_) | (_) | | (_| |  __/ | (_| | (_| | || (_) | | (_| | | | | |_| |
    |_| |_| .__/ \___/   \__,_|\___|  \__,_|\__,_|\__\___/   \__,_|_| |_|\__, |
          |_|                                                            |___/
 
*/
console.log('***** Tipo de dato any *****');
var vengador;
var existe;
console.log('existe: ', existe);
var derrotas;
console.log('derrotas: ', derrotas);
vengador = "Dr. Strange";
console.log('vengador: ', vengador);
console.log(vengador.charAt(0));
vengador = 150.5555;
console.log('vengador: ', vengador);
console.log(vengador.toFixed(2));
vengador = true;
console.log('vengador: ', vengador);
/*
 
      _                        _
     / \   _ __ _ __ ___  __ _| | ___  ___
    / _ \ | '__| '__/ _ \/ _` | |/ _ \/ __|
   / ___ \| |  | | |  __/ (_| | | (_) \__ \
  /_/   \_\_|  |_|  \___|\__, |_|\___/|___/
                         |___/
 
*/
console.log('***** Arreglos *****');
var arreglos = [1, 2, 3, 4];
var villanosMarvel = ["Omega Rojo", "Dormamu", "El Duende Verde"];
console.log(villanosMarvel[0].charAt(2));
/*
 
   _____            _
  |_   _|   _ _ __ | | __ _ ___
    | || | | | '_ \| |/ _` / __|
    | || |_| | |_) | | (_| \__ \
    |_| \__,_| .__/|_|\__,_|___/
             |_|
 
*/
console.log('***** Tuplas *****');
var tuplasHeroes = ["Dr. Strange", 100, true];
console.log('tuplasHeroes: ', tuplasHeroes);
/*
 
   _____                                           _
  | ____|_ __  _   _ _ __ ___   ___ _ __ __ _  ___(_) ___  _ __   ___  ___
  |  _| | '_ \| | | | '_ ` _ \ / _ \ '__/ _` |/ __| |/ _ \| '_ \ / _ \/ __|
  | |___| | | | |_| | | | | | |  __/ | | (_| | (__| | (_) | | | |  __/\__ \
  |_____|_| |_|\__,_|_| |_| |_|\___|_|  \__,_|\___|_|\___/|_| |_|\___||___/
                                                                           
 
*/
console.log('***** Enumeraciones *****');
var Volumen;
(function (Volumen) {
    Volumen[Volumen["min"] = 1] = "min";
    Volumen[Volumen["medio"] = 2] = "medio";
    Volumen[Volumen["max"] = 10] = "max";
})(Volumen || (Volumen = {}));
console.log('Volumen: ', Volumen);
var audio = Volumen.medio;
console.log('audio: ', audio);
/*
 
  __     __    _     _
  \ \   / /__ (_) __| |
   \ \ / / _ \| |/ _` |
    \ V / (_) | | (_| |
     \_/ \___/|_|\__,_|
                       
 
*/
console.log('***** Tipo de dato Void *****');
function llamar_batman() {
    console.log("Mostrar la batiseñal");
}
var mensaje = llamar_batman();
/*
 
   _   _
  | \ | | _____   _____ _ __
  |  \| |/ _ \ \ / / _ \ '__|
  | |\  |  __/\ V /  __/ |
  |_| \_|\___| \_/ \___|_|
                             
 
*/
console.log('***** Tipo de dato Never *****');
// function error(mensaje:string): never {
//     throw new Error(mensaje);
// }
// error("Error crítico... línea 201 alcanzada!");
/*
..######.....###.....######..########.########..#######.
.##....##...##.##...##....##....##....##.......##.....##
.##........##...##..##..........##....##.......##.....##
.##.......##.....##..######.....##....######...##.....##
.##.......#########.......##....##....##.......##.....##
.##....##.##.....##.##....##....##....##.......##.....##
..######..##.....##..######.....##....########..#######.
*/
console.log('***** Casteo de datos *****');
var cualquierCosa = "Cualquier cosa";
var largoDelString = cualquierCosa.length;
console.log('largoDelString: ' + largoDelString);
/** hola
lkjlkjj
kjhkjh
*/
var ar = [1, 2, 3];
for (var i = 0; i < ar.length; i++) {
    var element = ar[i];
    console.log(element);
}
