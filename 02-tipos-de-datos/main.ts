/*
  ____              _
 |  _ \            | |
 | |_) | ___   ___ | | ___  __ _ _ __   ___  ___
 |  _ < / _ \ / _ \| |/ _ \/ _` | '_ \ / _ \/ __|
 | |_) | (_) | (_) | |  __/ (_| | | | | (_) \__ \
 |____/ \___/ \___/|_|\___|\__,_|_| |_|\___/|___/


*/
console.log('***** Booleanos *****');
let esSuperman: boolean = true;
let esBAtman: boolean;
let esAcuaman: boolean = true;

if (esSuperman) {
    console.log("estamos salvados");
} else {
    console.log(" estamos pisados");
}

esSuperman = convertitClark();


function convertitClark() {
    return false;
}

/*
  _   _   __
 | \ | | /_/
 |  \| |_   _ _ __ ___   ___ _ __ ___  ___
 | . ` | | | | '_ ` _ \ / _ \ '__/ _ \/ __|
 | |\  | |_| | | | | | |  __/ | | (_) \__ \
 |_| \_|\__,_|_| |_| |_|\___|_|  \___/|___/


*/
console.log('***** Números *****');

let avengers: number = 5;
let villanos: number = 0;
let otros = 2;

if (avengers > villanos) {
    console.log("estamos salvados");
} else {
    console.log(" estamos pisados");
}

/*
 
    ____          _                            _                                 _                     
   / ___|__ _  __| | ___ _ __   __ _ ___    __| | ___    ___ __ _ _ __ __ _  ___| |_ ___ _ __ ___  ___ 
  | |   / _` |/ _` |/ _ \ '_ \ / _` / __|  / _` |/ _ \  / __/ _` | '__/ _` |/ __| __/ _ \ '__/ _ \/ __|
  | |__| (_| | (_| |  __/ | | | (_| \__ \ | (_| |  __/ | (_| (_| | | | (_| | (__| ||  __/ | |  __/\__ \
   \____\__,_|\__,_|\___|_| |_|\__,_|___/  \__,_|\___|  \___\__,_|_|  \__,_|\___|\__\___|_|  \___||___/
                                                                                                       
 
*/
console.log('***** Cadenas de caracteres *****');
let batman: string = "Batman";
let linternaVerde: string = 'Linterna Verde';
let volcanNegro: string = `Volcán Negro`

console.log(batman);
console.log(linternaVerde);
console.log(volcanNegro);

let concatenar: string = "Los héroes son: " + batman + ", " + linternaVerde + volcanNegro;
console.log(concatenar);

let concat: string = `Los héroes son ${batman}, ${linternaVerde}, ${volcanNegro}`
console.log(concat);

/*
 
   _____ _                   _            _       _                            
  |_   _(_)_ __   ___     __| | ___    __| | __ _| |_ ___     __ _ _ __  _   _ 
    | | | | '_ \ / _ \   / _` |/ _ \  / _` |/ _` | __/ _ \   / _` | '_ \| | | |
    | | | | |_) | (_) | | (_| |  __/ | (_| | (_| | || (_) | | (_| | | | | |_| |
    |_| |_| .__/ \___/   \__,_|\___|  \__,_|\__,_|\__\___/   \__,_|_| |_|\__, |
          |_|                                                            |___/ 
 
*/
console.log('***** Tipo de dato any *****');

let vengador: any;
let existe: any;
console.log('existe: ', existe);
let derrotas: any;
console.log('derrotas: ', derrotas);

vengador = "Dr. Strange";
console.log('vengador: ', vengador);
console.log(vengador.charAt(0));

vengador = 150.5555;
console.log('vengador: ', vengador);
console.log(vengador.toFixed(2));

vengador = true;
console.log('vengador: ', vengador);


/*
 
      _                        _           
     / \   _ __ _ __ ___  __ _| | ___  ___ 
    / _ \ | '__| '__/ _ \/ _` | |/ _ \/ __|
   / ___ \| |  | | |  __/ (_| | | (_) \__ \
  /_/   \_\_|  |_|  \___|\__, |_|\___/|___/
                         |___/             
 
*/
console.log('***** Arreglos *****');
let arreglos: number[] = [1, 2, 3, 4];
let villanosMarvel: string[] = ["Omega Rojo", "Dormamu", "El Duende Verde"];

console.log(villanosMarvel[0].charAt(2));

/*
 
   _____            _           
  |_   _|   _ _ __ | | __ _ ___ 
    | || | | | '_ \| |/ _` / __|
    | || |_| | |_) | | (_| \__ \
    |_| \__,_| .__/|_|\__,_|___/
             |_|                
 
*/
console.log('***** Tuplas *****');

let tuplasHeroes: [string, number, boolean] = ["Dr. Strange", 100, true];
console.log('tuplasHeroes: ', tuplasHeroes);

/*
 
   _____                                           _                       
  | ____|_ __  _   _ _ __ ___   ___ _ __ __ _  ___(_) ___  _ __   ___  ___ 
  |  _| | '_ \| | | | '_ ` _ \ / _ \ '__/ _` |/ __| |/ _ \| '_ \ / _ \/ __|
  | |___| | | | |_| | | | | | |  __/ | | (_| | (__| | (_) | | | |  __/\__ \
  |_____|_| |_|\__,_|_| |_| |_|\___|_|  \__,_|\___|_|\___/|_| |_|\___||___/
                                                                           
 
*/
console.log('***** Enumeraciones *****');

enum Volumen {
    min = 1,
    medio,
    max = 10,
}
console.log('Volumen: ', Volumen);

let audio: number = Volumen.medio;
console.log('audio: ', audio);

/*
 
  __     __    _     _ 
  \ \   / /__ (_) __| |
   \ \ / / _ \| |/ _` |
    \ V / (_) | | (_| |
     \_/ \___/|_|\__,_|
                       
 
*/
console.log('***** Tipo de dato Void *****');

function llamar_batman(): void {
    console.log("Mostrar la batiseñal");
}

let mensaje = llamar_batman();

/*
 
   _   _                     
  | \ | | _____   _____ _ __ 
  |  \| |/ _ \ \ / / _ \ '__|
  | |\  |  __/\ V /  __/ |   
  |_| \_|\___| \_/ \___|_|   
                             
 
*/
console.log('***** Tipo de dato Never *****');

// function error(mensaje:string): never {
//     throw new Error(mensaje);
// }

// error("Error crítico... línea 201 alcanzada!");

/*
   _____          _
  / ____|        | |
 | |     __ _ ___| |_ ___  ___
 | |    / _` / __| __/ _ \/ _ \
 | |___| (_| \__ \ ||  __/ (_) |
  \_____\__,_|___/\__\___|\___/


*/
console.log('***** Casteo de datos *****');

let cualquierCosa:any = "Cualquier cosa";
let largoDelString:number = (<string> cualquierCosa).length;

console.log('largoDelString: '+largoDelString);


/** hola
lkjlkjj
kjhkjh
*/

let ar = [1,2,3];

for (let i = 0; i < ar.length; i++) {
    const element = ar[i];
    console.log(element);
}


