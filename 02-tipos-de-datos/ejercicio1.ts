/**
 * Tipos
 */
let batman: string = "Bruce";
let superman: string = "Clark";

let existe: boolean = true;

/**
 * Tuplas
 */
let parejaHeroes: [string, string] = [batman, superman];
let villano: [string, number, boolean] = ["Lex Lutor", 5, true];

let aliados: string[] = ["Mujer Maravilla", "Acuaman", "San", "Flash"];

/**
 * Enumeraciones
 */
let fuerzaFlash: number = 5;
let fuerzaSuperman: number = 100;
let fuerzaBatman: number = 1;
let fuerzaAcuaman: number = 0;

enum fuerzaHeroes {
    fuerzaFlash = 5,
    fuerzaSuperman = 100,
    fuerzaBatman = 1,
    fuerzaAcuaman = 0,
}

/**
 * Retorno de funciones
 */
let activar_batisenial = (): string => {
    return "activada";
}

let pedir_ayuda = (): void => {
    console.log("Auxilio!!!");
}

/**
 * Aserciones de tipo
 */
let poder: string = "100";
let largoDelPoder: number = (<string>poder).length;
console.log("largoDelPoder", largoDelPoder)