// Funciones Básicas
// function sumar( a, b ){
//   return a + b;
// }

// Solución
function sumar(a: number, b: number): number {
  return a + b;
}

// var contar = function (heroes) {
//   return heroes.length;
// }
// var superHeroes = ["Flash", "Arrow", "Superman", "Linterna Verde"];
// contar(superHeroes);

let contar = function (heroes: string[]): number {
  return heroes.length;
}

let superHeroes: string[] = ["Flash", "Arrow", "Superman", "Linterna Verde"];
contar(superHeroes);

//Parametros por defecto
// function llamarBatman(llamar) {
//   if (llamar) {
//     console.log("Batiseñal activada");
//   }
// }

// llamarBatman();

function llamarBatman(llamar: boolean = true): void {
  if (llamar) {
    console.log("Batiseñal activada");
  }
}

llamarBatman();

// Rest?
function unirheroes(...personas: string[]) {
  return personas.join(", ");
}


// Tipo funcion
function noHaceNada(numero: number, texto: string, booleano: boolean, arreglo: any[]) {
}

// Crear el tipo de funcion que acepte la funcion "noHaceNada"
// var noHaceNadaTampoco;

// Solucion
let noHaceNadaTampoco: (num: number, tex: string, boo: boolean, arr: any[]) => void;
noHaceNadaTampoco = noHaceNada;
