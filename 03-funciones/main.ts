/*
╔═╗┬ ┬┌┐┌┌─┐┬┌─┐┌┐┌┌─┐┌─┐  ╔╗ ┌─┐┌─┐┬┌─┐┌─┐┌─┐
╠╣ │ │││││  ││ ││││├┤ └─┐  ╠╩╗├─┤└─┐││  ├─┤└─┐
╚  └─┘┘└┘└─┘┴└─┘┘└┘└─┘└─┘  ╚═╝┴ ┴└─┘┴└─┘┴ ┴└─┘
*/
console.log("********** Funciones Básicas **********");
let heroe: string = "Flash";

function imprimeHeroe(): string {
    return heroe;
}

let activarBatisenial = function (): string {
    return "Batiseñal activada";
}

console.log(imprimeHeroe());
console.log(activarBatisenial());

/*
╔═╗┌─┐┬─┐┌─┐┌┬┐┌─┐┌┬┐┬─┐┌─┐┌─┐  ┌─┐┌┐ ┬  ┬┌─┐┌─┐┌┬┐┌─┐┬─┐┬┌─┐┌─┐
╠═╝├─┤├┬┘├─┤│││├┤  │ ├┬┘│ │└─┐  │ │├┴┐│  ││ ┬├─┤ │ │ │├┬┘││ │└─┐
╩  ┴ ┴┴└─┴ ┴┴ ┴└─┘ ┴ ┴└─└─┘└─┘  └─┘└─┘┴─┘┴└─┘┴ ┴ ┴ └─┘┴└─┴└─┘└─┘
*/
console.log("********** Parámetros Obligatorios **********");
function nombreCompleto1(nombre: string, apellido: string): string {
    return `${nombre} ${apellido}`;
}

let nombre = nombreCompleto1("Clark", "Kent");

console.log(nombre);

/*
╔═╗┌─┐┬─┐┌─┐┌┬┐┌─┐┌┬┐┬─┐┌─┐┌─┐  ┌─┐┌─┐┌─┐┬┌─┐┌┐┌┌─┐┬  ┌─┐┌─┐
╠═╝├─┤├┬┘├─┤│││├┤  │ ├┬┘│ │└─┐  │ │├─┘│  ││ ││││├─┤│  ├┤ └─┐
╩  ┴ ┴┴└─┴ ┴┴ ┴└─┘ ┴ ┴└─└─┘└─┘  └─┘┴  └─┘┴└─┘┘└┘┴ ┴┴─┘└─┘└─┘
*/
console.log("********** Parámetros Opcionales **********");
function nombreCompleto2(nombre: string, apellido?: string): string {
    if (apellido) {

        return `${nombre} ${apellido}`;
    } else {
        return nombre
    }
}

let nombres = nombreCompleto2("Barry", "Allen");

console.log(nombres);

/*
╔═╗┌─┐┬─┐┌─┐┌┬┐┌─┐┌┬┐┬─┐┌─┐┌─┐  ┌─┐┌─┐┬─┐  ┌┬┐┌─┐┌─┐┌─┐┌─┐┌┬┐┌─┐
╠═╝├─┤├┬┘├─┤│││├┤  │ ├┬┘│ │└─┐  ├─┘│ │├┬┘   ││├┤ ├┤ ├┤ │   │ │ │
╩  ┴ ┴┴└─┴ ┴┴ ┴└─┘ ┴ ┴└─└─┘└─┘  ┴  └─┘┴└─  ─┴┘└─┘└  └─┘└─┘ ┴ └─┘
*/
console.log("********** Parámetros por Defecto **********");
function nombreCompleto3(nombre: string, apellido: string, capitalizado: boolean = false): string {
    if (capitalizado) {
        return `${capitalizar(nombre)} ${capitalizar(apellido)}`;

    } else {
        return `${nombre} ${apellido}`;
    }
}

function capitalizar(palabra: string): string {
    return palabra.charAt(0).toUpperCase() + palabra.substr(1).toLowerCase();
}

let names = nombreCompleto3("barry", "allen");

console.log(names);

/*
╔═╗┌─┐┬─┐┌─┐┌┬┐┌─┐┌┬┐┬─┐┌─┐┌─┐  ╦═╗╔═╗╔═╗╔╦╗
╠═╝├─┤├┬┘├─┤│││├┤  │ ├┬┘│ │└─┐  ╠╦╝║╣ ╚═╗ ║
╩  ┴ ┴┴└─┴ ┴┴ ┴└─┘ ┴ ┴└─└─┘└─┘  ╩╚═╚═╝╚═╝ ╩
*/
console.log("********** Parámetros REST **********");

function nombreCompleto4(nombre: string, ...losDemasNombres: string[]): string {
    return `${nombre} ${losDemasNombres.join(" ")}`
}

let superman: string = nombreCompleto4("Clark", "Joseph", "Kent");
console.log("superman", superman)
let ironman: string = nombreCompleto4("Anthony", "Edward", "'tony'", "Stark");
console.log("ironman", ironman)

/*
╔╦╗┬┌─┐┌─┐  ╔═╗┬ ┬┌┐┌┌─┐┬┌─┐┌┐┌
 ║ │├─┘│ │  ╠╣ │ │││││  ││ ││││
 ╩ ┴┴  └─┘  ╚  └─┘┘└┘└─┘┴└─┘┘└┘
*/
console.log("********** Tipo Función **********");

function sumando(a: number, b: number): number {
    return a + b;
}

function saludar(nombre: string): string {
    return "Yo soy " + nombre;
}

function salvarMundo():void {
    console.log("el mundo esta ha salvo");
}

/**
 * Reglas para definir el tipo función
 */ 
// let miFuncion: (x:number, y:number) => number ;
// let miFuncion: (y:string) => string ;
// let miFuncion: () => void ;
let miFuncion;

miFuncion = 10;
console.log(miFuncion);

miFuncion  = sumando;
console.log(miFuncion(5,5));

miFuncion = saludar;
console.log(miFuncion("Batman"));

miFuncion = salvarMundo;
miFuncion();