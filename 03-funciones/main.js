"use strict";
/*
╔═╗┬ ┬┌┐┌┌─┐┬┌─┐┌┐┌┌─┐┌─┐  ╔╗ ┌─┐┌─┐┬┌─┐┌─┐┌─┐
╠╣ │ │││││  ││ ││││├┤ └─┐  ╠╩╗├─┤└─┐││  ├─┤└─┐
╚  └─┘┘└┘└─┘┴└─┘┘└┘└─┘└─┘  ╚═╝┴ ┴└─┘┴└─┘┴ ┴└─┘
*/
console.log("********** Funciones Básicas **********");
var heroe = "Flash";
function imprimeHeroe() {
    return heroe;
}
var activarBatisenial = function () {
    return "Batiseñal activada";
};
console.log(imprimeHeroe());
console.log(activarBatisenial());
/*
╔═╗┌─┐┬─┐┌─┐┌┬┐┌─┐┌┬┐┬─┐┌─┐┌─┐  ┌─┐┌┐ ┬  ┬┌─┐┌─┐┌┬┐┌─┐┬─┐┬┌─┐┌─┐
╠═╝├─┤├┬┘├─┤│││├┤  │ ├┬┘│ │└─┐  │ │├┴┐│  ││ ┬├─┤ │ │ │├┬┘││ │└─┐
╩  ┴ ┴┴└─┴ ┴┴ ┴└─┘ ┴ ┴└─└─┘└─┘  └─┘└─┘┴─┘┴└─┘┴ ┴ ┴ └─┘┴└─┴└─┘└─┘
*/
console.log("********** Parámetros Obligatorios **********");
function nombreCompleto1(nombre, apellido) {
    return nombre + " " + apellido;
}
var nombre = nombreCompleto1("Clark", "Kent");
console.log(nombre);
/*
╔═╗┌─┐┬─┐┌─┐┌┬┐┌─┐┌┬┐┬─┐┌─┐┌─┐  ┌─┐┌─┐┌─┐┬┌─┐┌┐┌┌─┐┬  ┌─┐┌─┐
╠═╝├─┤├┬┘├─┤│││├┤  │ ├┬┘│ │└─┐  │ │├─┘│  ││ ││││├─┤│  ├┤ └─┐
╩  ┴ ┴┴└─┴ ┴┴ ┴└─┘ ┴ ┴└─└─┘└─┘  └─┘┴  └─┘┴└─┘┘└┘┴ ┴┴─┘└─┘└─┘
*/
console.log("********** Parámetros Opcionales **********");
function nombreCompleto2(nombre, apellido) {
    if (apellido) {
        return nombre + " " + apellido;
    }
    else {
        return nombre;
    }
}
var nombres = nombreCompleto2("Barry", "Allen");
console.log(nombres);
/*
╔═╗┌─┐┬─┐┌─┐┌┬┐┌─┐┌┬┐┬─┐┌─┐┌─┐  ┌─┐┌─┐┬─┐  ┌┬┐┌─┐┌─┐┌─┐┌─┐┌┬┐┌─┐
╠═╝├─┤├┬┘├─┤│││├┤  │ ├┬┘│ │└─┐  ├─┘│ │├┬┘   ││├┤ ├┤ ├┤ │   │ │ │
╩  ┴ ┴┴└─┴ ┴┴ ┴└─┘ ┴ ┴└─└─┘└─┘  ┴  └─┘┴└─  ─┴┘└─┘└  └─┘└─┘ ┴ └─┘
*/
console.log("********** Parámetros por Defecto **********");
function nombreCompleto3(nombre, apellido, capitalizado) {
    if (capitalizado === void 0) { capitalizado = false; }
    if (capitalizado) {
        return capitalizar(nombre) + " " + capitalizar(apellido);
    }
    else {
        return nombre + " " + apellido;
    }
}
function capitalizar(palabra) {
    return palabra.charAt(0).toUpperCase() + palabra.substr(1).toLowerCase();
}
var names = nombreCompleto3("barry", "allen");
console.log(names);
/*
╔═╗┌─┐┬─┐┌─┐┌┬┐┌─┐┌┬┐┬─┐┌─┐┌─┐  ╦═╗╔═╗╔═╗╔╦╗
╠═╝├─┤├┬┘├─┤│││├┤  │ ├┬┘│ │└─┐  ╠╦╝║╣ ╚═╗ ║
╩  ┴ ┴┴└─┴ ┴┴ ┴└─┘ ┴ ┴└─└─┘└─┘  ╩╚═╚═╝╚═╝ ╩
*/
console.log("********** Parámetros REST **********");
function nombreCompleto4(nombre) {
    var losDemasNombres = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        losDemasNombres[_i - 1] = arguments[_i];
    }
    return nombre + " " + losDemasNombres.join(" ");
}
var superman = nombreCompleto4("Clark", "Joseph", "Kent");
console.log("superman", superman);
var ironman = nombreCompleto4("Anthony", "Edward", "'tony'", "Stark");
console.log("ironman", ironman);
/*
╔╦╗┬┌─┐┌─┐  ╔═╗┬ ┬┌┐┌┌─┐┬┌─┐┌┐┌
 ║ │├─┘│ │  ╠╣ │ │││││  ││ ││││
 ╩ ┴┴  └─┘  ╚  └─┘┘└┘└─┘┴└─┘┘└┘
*/
console.log("********** Tipo Función **********");
function sumando(a, b) {
    return a + b;
}
function saludar(nombre) {
    return "Yo soy " + nombre;
}
function salvarMundo() {
    console.log("el mundo esta ha salvo");
}
/**
 * Reglas para definir el tipo función
 */
// let miFuncion: (x:number, y:number) => number ;
// let miFuncion: (y:string) => string ;
// let miFuncion: () => void ;
var miFuncion;
miFuncion = 10;
console.log(miFuncion);
miFuncion = sumando;
console.log(miFuncion(5, 5));
miFuncion = saludar;
console.log(miFuncion("Batman"));
miFuncion = salvarMundo;
miFuncion();
