/*
╦┌┐┌┌┬┐┌─┐┬─┐┌─┐┌─┐┌─┐┌─┐┌─┐  ┌┐ ┌─┐┌─┐┬┌─┐┌─┐┌─┐
║│││ │ ├┤ ├┬┘├┤ ├─┤│  ├┤ └─┐  ├┴┐├─┤└─┐││  ├─┤└─┐
╩┘└┘ ┴ └─┘┴└─└  ┴ ┴└─┘└─┘└─┘  └─┘┴ ┴└─┘┴└─┘┴ ┴└─┘
*/
console.log('********** Interfaces básicas **********');
interface Xmen {
    nombre: string;
    poder?: string;
    nombreReal?: string;
    regenerar(nombreReal: string): void;
}

class Mutante implements Xmen {
    nombre: string;
    esBueno: boolean;
    regenerar(nombre: string) {
        console.log(`Hola ${nombre}`);
    };

    constructor(nombre: string, esBueno: boolean) {
        this.nombre = nombre;
        this.esBueno = esBueno;

    }
}

function enviarMision(xmen: Xmen): void {
    console.log("Enviando a misión a " + xmen.nombre);

    xmen.regenerar("Logan");
}

function enviarCuartel(xmen: Xmen): void {
    console.log("Enviando al cuartel a: " + xmen.nombre);
}

let wolverine = {
    nombre: "Wolverine",
    // poder: "Regeneración",
    regenerar(x: string) {
        console.log("Se ha regenerado " + x);
    }
}

enviarMision(wolverine);
enviarCuartel(wolverine);

let cyclops = new Mutante("Scott", true);
cyclops.regenerar(cyclops.nombre);

/*
┬┌┐┌┌┬┐┌─┐┬─┐┌─┐┌─┐┌─┐┌─┐┌─┐  ┌─┐┌┐┌  ┌─┐┬ ┬┌┐┌┌─┐┬┌─┐┌┐┌┌─┐┌─┐
││││ │ ├┤ ├┬┘├┤ ├─┤│  ├┤ └─┐  ├┤ │││  ├┤ │ │││││  ││ ││││├┤ └─┐
┴┘└┘ ┴ └─┘┴└─└  ┴ ┴└─┘└─┘└─┘  └─┘┘└┘  └  └─┘┘└┘└─┘┴└─┘┘└┘└─┘└─┘
*/
interface DosNumerosFunc {
    (nume1: number, num2: number): number
}

let sumar: DosNumerosFunc;
sumar = (a: number, b: number): number => {
    return a + b;
}

let restar: DosNumerosFunc;
restar = function (x: number, y: number): number {
    return x - y;
}