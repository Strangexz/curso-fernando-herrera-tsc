// Crear interfaces
interface UsuarioAkumaNoMi {
  nombreUsuario: string;
  nombreFruta: string;
  poder: string;
  habilidad?: string
}


// Cree una interfaz para validar el auto (el valor enviado por parametro)
interface ValidarAuto {
  (auto: Auto): void
}

interface Auto {
  encender: boolean;
  velocidadMaxima: number;
  acelerar(): void;
}

let conducirBatimovil: ValidarAuto;
conducirBatimovil = (auto: Auto): void => {
  auto.encender = true;
  auto.velocidadMaxima = 100;
  auto.acelerar();
}

let batimovil: Auto = {
  encender: false,
  velocidadMaxima: 0,
  acelerar() {
    console.log("...... run!!!");
  }
}

// Cree una interfaz con que permita utilzar el siguiente objeto
// utilizando propiedades opcionales
interface Joker {
  reir: boolean;
  comer: boolean;
  llorar?: boolean
}

let guason: Joker = {
  reir: true,
  comer: true,
  llorar: false
}

function reir(guason: Joker): void {
  if (guason.reir) {
    console.log("JAJAJAJA");
  }
}


// Cree una interfaz para la siguiente funcion
interface Ciudad {
  (ciudadanos: string[]): number
}

let ciudadGotica: Ciudad;
ciudadGotica = (ciudadanos: string[]): number => {
  return ciudadanos.length;
}

// Cree una interfaz que obligue crear una clase
// con las siguientes propiedades y metodos

/*
  propiedades:
    - nombre
    - edad
    - sexo
    - estadoCivil
    - imprimirBio(): void // en consola una breve descripcion.
*/

interface Persona {
  nombre: string;
  edad: number;
  sexo?: string;
  estadoCivil?: string;
  imprimirBio(): void;
}

class Ciudadano implements Persona {
  nombre: string;
  edad: number;
  sexo?: string;
  estadoCivil?: string;
  imprimirBio(): void {
    console.log(`Hola yo soy ${this.nombre}`);
  }

  constructor(nombre: string, edad: number) {
    this.nombre = nombre;
    this.edad = edad;
  }
}

let juan = new Ciudadano("Juan", 34);
juan.imprimirBio();
