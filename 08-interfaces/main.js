"use strict";
/*
╦┌┐┌┌┬┐┌─┐┬─┐┌─┐┌─┐┌─┐┌─┐┌─┐  ┌┐ ┌─┐┌─┐┬┌─┐┌─┐┌─┐
║│││ │ ├┤ ├┬┘├┤ ├─┤│  ├┤ └─┐  ├┴┐├─┤└─┐││  ├─┤└─┐
╩┘└┘ ┴ └─┘┴└─└  ┴ ┴└─┘└─┘└─┘  └─┘┴ ┴└─┘┴└─┘┴ ┴└─┘
*/
console.log('********** Interfaces básicas **********');
var Mutante = /** @class */ (function () {
    function Mutante(nombre, esBueno) {
        this.nombre = nombre;
        this.esBueno = esBueno;
    }
    Mutante.prototype.regenerar = function (nombre) {
        console.log("Hola " + nombre);
    };
    ;
    return Mutante;
}());
function enviarMision(xmen) {
    console.log("Enviando a misión a " + xmen.nombre);
    xmen.regenerar("Logan");
}
function enviarCuartel(xmen) {
    console.log("Enviando al cuartel a: " + xmen.nombre);
}
var wolverine = {
    nombre: "Wolverine",
    // poder: "Regeneración",
    regenerar: function (x) {
        console.log("Se ha regenerado " + x);
    }
};
enviarMision(wolverine);
enviarCuartel(wolverine);
var cyclops = new Mutante("Scott", true);
cyclops.regenerar(cyclops.nombre);
var sumar;
sumar = function (a, b) {
    return a + b;
};
var restar;
restar = function (x, y) {
    return x - y;
};
