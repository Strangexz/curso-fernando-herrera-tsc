"use strict";
var conducirBatimovil;
conducirBatimovil = function (auto) {
    auto.encender = true;
    auto.velocidadMaxima = 100;
    auto.acelerar();
};
var batimovil = {
    encender: false,
    velocidadMaxima: 0,
    acelerar: function () {
        console.log("...... run!!!");
    }
};
var guason = {
    reir: true,
    comer: true,
    llorar: false
};
function reir(guason) {
    if (guason.reir) {
        console.log("JAJAJAJA");
    }
}
var ciudadGotica;
ciudadGotica = function (ciudadanos) {
    return ciudadanos.length;
};
var Ciudadano = /** @class */ (function () {
    function Ciudadano(nombre, edad) {
        this.nombre = nombre;
        this.edad = edad;
    }
    Ciudadano.prototype.imprimirBio = function () {
        console.log("Hola yo soy " + this.nombre);
    };
    return Ciudadano;
}());
var juan = new Ciudadano("Juan", 34);
juan.imprimirBio();
