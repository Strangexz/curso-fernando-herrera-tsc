const MENSAJES: string[] = [
    "El texto es muy corto",
    "El texto es muy largo"
];

export function obtenerError(numErr: number): string {
    return MENSAJES[numErr];
}