/*
╔═╗┌┐  ┬┌─┐┌┬┐┌─┐  ╔╗ ┌─┐┌─┐┬┌─┐┌─┐
║ ║├┴┐ │├┤  │ │ │  ╠╩╗├─┤└─┐││  │ │
╚═╝└─┘└┘└─┘ ┴ └─┘  ╚═╝┴ ┴└─┘┴└─┘└─┘
*/

let superHeroe = {
    nombre: "Barry Allen",
    edad: 24,
    poderes: [
        "Super velocidad",
        "Viajar por el tiempo"
    ]
};

superHeroe = {
    nombre: "Clark Kent",
    edad: 500,
    poderes: [
        "Volar"
    ]
};

/*
╔╦╗┌─┐┌─┐┬┌┐┌┬┌─┐┌┐┌┌┬┐┌─┐  ┌─┐┌┐  ┬┌─┐┌┬┐┌─┐┌─┐  ┌┬┐┌─┐  ┌┬┐┬┌─┐┌─┐  ┌─┐┌─┐┌─┐┌─┐┌─┐┬┌─┐┬┌─┐┌─┐
 ║║├┤ ├┤ │││││├┤ │││ │││ │  │ │├┴┐ │├┤  │ │ │└─┐   ││├┤    │ │├─┘│ │  ├┤ └─┐├─┘├┤ │  │├┤ ││  │ │
═╩╝└─┘└  ┴┘└┘┴└─┘┘└┘─┴┘└─┘  └─┘└─┘└┘└─┘ ┴ └─┘└─┘  ─┴┘└─┘   ┴ ┴┴  └─┘  └─┘└─┘┴  └─┘└─┘┴└  ┴└─┘└─┘
*/
let heroe: { nombre: string, edad: number, poderes: string[] } = {
    nombre: "Barry Allen",
    edad: 24,
    poderes: [
        "Super velocidad",
        "Viajar por el tiempo"
    ]
}

/*
╔╦╗┌─┐┌┬┐┌─┐┌┬┐┌─┐┌─┐  ┌┬┐┌─┐┌┐┌┌┬┐┬─┐┌─┐  ┌┬┐┌─┐  ┌─┐┌┐  ┬┌─┐┌┬┐┌─┐┌─┐
║║║├┤  │ │ │ │││ │└─┐   ││├┤ │││ │ ├┬┘│ │   ││├┤   │ │├┴┐ │├┤  │ │ │└─┐
╩ ╩└─┘ ┴ └─┘─┴┘└─┘└─┘  ─┴┘└─┘┘└┘ ┴ ┴└─└─┘  ─┴┘└─┘  └─┘└─┘└┘└─┘ ┴ └─┘└─┘
*/
let heroe2: { nombre: string, edad: number, poderes: string[], getNombre: () => string } = {
    nombre: "Barry Allen",
    edad: 24,
    poderes: [
        "Super velocidad",
        "Viajar por el tiempo"
    ],
    getNombre() {
        return this.nombre;
    }
}

heroe2.getNombre();

/*
╔╦╗┬┌─┐┌─┐┌─┐  ┌─┐┌─┐┬─┐┌─┐┌─┐┌┐┌┌─┐┬  ┬┌─┐┌─┐┌┬┐┌─┐┌─┐
 ║ │├─┘│ │└─┐  ├─┘├┤ ├┬┘└─┐│ ││││├─┤│  │┌─┘├─┤ │││ │└─┐
 ╩ ┴┴  └─┘└─┘  ┴  └─┘┴└─└─┘└─┘┘└┘┴ ┴┴─┘┴└─┘┴ ┴─┴┘└─┘└─┘
*/
type Heroe = {
    nombre: string,
    edad: number,
    poderes: string[],
    getNombre: () => string
}

let flash: Heroe = {
    nombre: "Barry Allen",
    edad: 24,
    poderes: [
        "Super velocidad",
        "Viajar por el tiempo"
    ],
    getNombre() {
        return this.nombre;
    }
}

let superman: Heroe = {
    nombre: "Clark Kent",
    edad: 500,
    poderes: [
        "Super velocidad",
        "Volar"
    ],
    getNombre() {
        return this.nombre;
    }
}

/*
╔╦╗┬ ┬┬ ┌┬┐┬┌─┐┬  ┌─┐┌─┐  ╔╦╗┬┌─┐┌─┐┌─┐  ╔═╗┌─┐┬─┐┌┬┐┬┌┬┐┬┌┬┐┌─┐┌─┐
║║║│ ││  │ │├─┘│  ├┤ └─┐   ║ │├─┘│ │└─┐  ╠═╝├┤ ├┬┘││││ │ │ │││ │└─┐
╩ ╩└─┘┴─┘┴ ┴┴  ┴─┘└─┘└─┘   ╩ ┴┴  └─┘└─┘  ╩  └─┘┴└─┴ ┴┴ ┴ ┴─┴┘└─┘└─┘
*/
let loquesea: string | number | Heroe;

loquesea = 10;
loquesea = "fernando";
loquesea = {
    nombre: "Flash",
    edad: 56,
    poderes: [
        "Super velocidad",
        "Viajar por el tiempo"
    ],
    getNombre() {
        return "";
    }
};

/*
╦  ╦┌─┐┬─┐┬┌─┐┬┌─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┌┬┐┬┌─┐┌─┐  ┌┬┐┌─┐  ┌┬┐┌─┐┌┬┐┌─┐┌─┐
╚╗╔╝├┤ ├┬┘│├┤ ││  ├─┤│  ││ ││││   ││├┤    │ │├─┘│ │   ││├┤    ││├─┤ │ │ │└─┐
 ╚╝ └─┘┴└─┴└  ┴└─┘┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘   ┴ ┴┴  └─┘  ─┴┘└─┘  ─┴┘┴ ┴ ┴ └─┘└─┘
*/
let cosa:any = ():void => {};

console.log(typeof cosa);

if (typeof cosa === 'number') {
    console.log("cosa es un número");
} else {
    console.log("cosa no es un número");
}
