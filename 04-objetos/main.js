"use strict";
/*
╔═╗┌┐  ┬┌─┐┌┬┐┌─┐  ╔╗ ┌─┐┌─┐┬┌─┐┌─┐
║ ║├┴┐ │├┤  │ │ │  ╠╩╗├─┤└─┐││  │ │
╚═╝└─┘└┘└─┘ ┴ └─┘  ╚═╝┴ ┴└─┘┴└─┘└─┘
*/
var superHeroe = {
    nombre: "Barry Allen",
    edad: 24,
    poderes: [
        "Super velocidad",
        "Viajar por el tiempo"
    ]
};
superHeroe = {
    nombre: "Clark Kent",
    edad: 500,
    poderes: [
        "Volar"
    ]
};
/*
╔╦╗┌─┐┌─┐┬┌┐┌┬┌─┐┌┐┌┌┬┐┌─┐  ┌─┐┌┐  ┬┌─┐┌┬┐┌─┐┌─┐  ┌┬┐┌─┐  ┌┬┐┬┌─┐┌─┐  ┌─┐┌─┐┌─┐┌─┐┌─┐┬┌─┐┬┌─┐┌─┐
 ║║├┤ ├┤ │││││├┤ │││ │││ │  │ │├┴┐ │├┤  │ │ │└─┐   ││├┤    │ │├─┘│ │  ├┤ └─┐├─┘├┤ │  │├┤ ││  │ │
═╩╝└─┘└  ┴┘└┘┴└─┘┘└┘─┴┘└─┘  └─┘└─┘└┘└─┘ ┴ └─┘└─┘  ─┴┘└─┘   ┴ ┴┴  └─┘  └─┘└─┘┴  └─┘└─┘┴└  ┴└─┘└─┘
*/
var heroe = {
    nombre: "Barry Allen",
    edad: 24,
    poderes: [
        "Super velocidad",
        "Viajar por el tiempo"
    ]
};
/*
╔╦╗┌─┐┌┬┐┌─┐┌┬┐┌─┐┌─┐  ┌┬┐┌─┐┌┐┌┌┬┐┬─┐┌─┐  ┌┬┐┌─┐  ┌─┐┌┐  ┬┌─┐┌┬┐┌─┐┌─┐
║║║├┤  │ │ │ │││ │└─┐   ││├┤ │││ │ ├┬┘│ │   ││├┤   │ │├┴┐ │├┤  │ │ │└─┐
╩ ╩└─┘ ┴ └─┘─┴┘└─┘└─┘  ─┴┘└─┘┘└┘ ┴ ┴└─└─┘  ─┴┘└─┘  └─┘└─┘└┘└─┘ ┴ └─┘└─┘
*/
var heroe2 = {
    nombre: "Barry Allen",
    edad: 24,
    poderes: [
        "Super velocidad",
        "Viajar por el tiempo"
    ],
    getNombre: function () {
        return this.nombre;
    }
};
heroe2.getNombre();
var flash = {
    nombre: "Barry Allen",
    edad: 24,
    poderes: [
        "Super velocidad",
        "Viajar por el tiempo"
    ],
    getNombre: function () {
        return this.nombre;
    }
};
var superman = {
    nombre: "Clark Kent",
    edad: 500,
    poderes: [
        "Super velocidad",
        "Volar"
    ],
    getNombre: function () {
        return this.nombre;
    }
};
/*
╔╦╗┬ ┬┬ ┌┬┐┬┌─┐┬  ┌─┐┌─┐  ╔╦╗┬┌─┐┌─┐┌─┐  ╔═╗┌─┐┬─┐┌┬┐┬┌┬┐┬┌┬┐┌─┐┌─┐
║║║│ ││  │ │├─┘│  ├┤ └─┐   ║ │├─┘│ │└─┐  ╠═╝├┤ ├┬┘││││ │ │ │││ │└─┐
╩ ╩└─┘┴─┘┴ ┴┴  ┴─┘└─┘└─┘   ╩ ┴┴  └─┘└─┘  ╩  └─┘┴└─┴ ┴┴ ┴ ┴─┴┘└─┘└─┘
*/
var loquesea;
loquesea = 10;
loquesea = "fernando";
loquesea = {
    nombre: "Flash",
    edad: 56,
    poderes: [
        "Super velocidad",
        "Viajar por el tiempo"
    ],
    getNombre: function () {
        return "";
    }
};
/*
╦  ╦┌─┐┬─┐┬┌─┐┬┌─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┌┬┐┬┌─┐┌─┐  ┌┬┐┌─┐  ┌┬┐┌─┐┌┬┐┌─┐┌─┐
╚╗╔╝├┤ ├┬┘│├┤ ││  ├─┤│  ││ ││││   ││├┤    │ │├─┘│ │   ││├┤    ││├─┤ │ │ │└─┐
 ╚╝ └─┘┴└─┴└  ┴└─┘┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘   ┴ ┴┴  └─┘  ─┴┘└─┘  ─┴┘┴ ┴ ┴ └─┘└─┘
*/
var cosa = function () { };
console.log(typeof cosa);
if (typeof cosa === 'number') {
    console.log("cosa es un número");
}
else {
    console.log("cosa no es un número");
}
