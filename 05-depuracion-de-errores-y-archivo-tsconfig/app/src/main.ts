/*!
 * Derechos de autor: strangexz
 * Version 0.0.1
 */
/*
╔╦╗┌─┐┌─┐┬ ┬┬─┐┌─┐┌─┐┬┌─┐┌┐┌  ┌┬┐┌─┐  ┌─┐┬─┐┬─┐┌─┐┬─┐┌─┐┌─┐
 ║║├┤ ├─┘│ │├┬┘├─┤│  ││ ││││   ││├┤   ├┤ ├┬┘├┬┘│ │├┬┘├┤ └─┐
═╩╝└─┘┴  └─┘┴└─┴ ┴└─┘┴└─┘┘└┘  ─┴┘└─┘  └─┘┴└─┴└─└─┘┴└─└─┘└─┘
*/

/**
 * Comentario multilinea
 */
let heroe: string = "Ricardo Tapia (Robin)";
let edad: number = 30;

// heroe = edad;
console.log(heroe);
imprimir(heroe, edad);

/**
 * Función que imprime el nombre y la edad del heroe.
 * No devuelve nada
 * 
 * @param heroe string - nombre del heroe
 * @param edad number - edad del heroe
 * 
 */
function imprimir(heroe: string, edad: number): void {
    heroe = heroe.toLowerCase();
    console.log("imprimir -> heroe", heroe)
    edad = edad + 10;
    console.log("imprimir -> edad", edad)
}

// let prueba:number = undefined;