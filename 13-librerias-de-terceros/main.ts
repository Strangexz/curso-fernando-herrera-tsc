import "jquery";
import "sw";

console.log("Hola mundo");

$(document).ready(function () {
    console.log("página lista y cargada");

    $("h1").text("Hola desde typescript");
    $("p").text("Hola desde este parrafo...");
    $("h1").css("background-color", "blue");
    $("h1").css("color", "white");

});

$("#btnAlerta").on("click", function () {
    // alert("Hola desde typescript")
    swal("Titulo", "mensaje", "success");
});

